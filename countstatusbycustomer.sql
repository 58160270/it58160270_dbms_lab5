DROP PROCEDURE IF EXISTS CountStatusByCustomer ;
DELIMITER $$

CREATE PROCEDURE CountStatusByCustomer(IN shipDate DATE , IN customer INT(11) , OUT Total INT ) 
BEGIN
	select count(status)
	INTO Total
	from orders where shippedDate=shipDate AND customerNumber=customer AND status='Shipped';
END $$
DELIMITER ;
