DROP PROCEDURE IF EXISTS CountStatus;
DELIMITER $$

CREATE PROCEDURE CountStatus
(IN Orderstatus VARCHAR(15), OUT Total INT )
BEGIN
	 select count(orderNumber)
   	 INTO Total
	 from orders where status=OrderStatus;

END $$

DELIMITER ;
